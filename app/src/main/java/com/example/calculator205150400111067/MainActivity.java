package com.example.calculator205150400111067;

import static android.provider.AlarmClock.EXTRA_MESSAGE;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String EXTRA_MESSAGE = "com.example.calculator205150400111067.MESSAGE";

    TextView tvHasil, tvOperasi;
    Button btnBagi, btnKali, btnTambah, btnKurang, btnSamaDengan;
    Button btn0, btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8, btn9;
    Button btnC, btnTitik;

    String s0 = "";
    String s1 = "";
    String s2 = "";
    String hasilFix ="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tvOperasi = findViewById(R.id.textView_operasi);

        assignId(btnBagi, R.id.button_bagi);
        assignId(btnKali, R.id.button_kali);
        assignId(btnTambah, R.id.button_tambah);
        assignId(btnKurang, R.id.button_kurang);
        assignId(btnSamaDengan, R.id.button_samaDengan);

        assignId(btn0, R.id.button_nol);
        assignId(btn1, R.id.button_satu);
        assignId(btn2, R.id.button_dua);
        assignId(btn3, R.id.button_tiga);
        assignId(btn4, R.id.button_empat);
        assignId(btn5, R.id.button_lima);
        assignId(btn6, R.id.button_enam);
        assignId(btn7, R.id.button_tujuh);
        assignId(btn8, R.id.button_delapan);
        assignId(btn9, R.id.button_sembilan);

        assignId(btnC, R.id.button_c);
        assignId(btnTitik, R.id.button_titik);
    }

    void assignId(Button btn, int id) {
        btn = findViewById(id);
        btn.setOnClickListener(this::onClick);
    }

    @Override
    public void onClick(View view) {
        Button btn = (Button) view;
        String btnTxt = btn.getText().toString();
        tvOperasi.setText(s0+s1+s2);

        if (btnTxt.charAt(0) >= '0' && btnTxt.charAt(0) <= '9' || btnTxt.charAt(0) == '.') {
            if (!s1.equals("")) {
                s2 = s2 + btnTxt;
            }
            else {
                s0 = s0 + btnTxt;
            }
            tvOperasi.setText(s0+s1+s2);
            return;
        }

        if (btnTxt.equals("C")) {
            hasilFix = "0";
            s0 = s1 = s2 = "";
            tvOperasi.setText(s0+s1+s2);
            return;
        }

        if (btnTxt.equals("=")) {
            if (!s0.equals("") && !s1.equals("") && !s2.equals("")) {
                double hasil;
                switch (s1) {
                    case "+": hasil = Double.parseDouble(s0)+Double.parseDouble(s2); break;
                    case "-": hasil = Double.parseDouble(s0)-Double.parseDouble(s2); break;
                    case "/": hasil = Double.parseDouble(s0)/Double.parseDouble(s2); break;
                    case "*": hasil = Double.parseDouble(s0)*Double.parseDouble(s2); break;
                    default:
                        throw new IllegalStateException("Unexpected value: " + s1);
                }

                hasilFix = Double.toString(hasil);
                if (hasilFix.substring(hasilFix.length()-2).equals(".0")) {
                    hasilFix = hasilFix.substring(0, hasilFix.length()-2);
                }

                Intent intent = new Intent(this, DisplayMessageActivity.class);
                intent.putExtra(EXTRA_MESSAGE, (s0+s1+s2+" = " + hasilFix));
                startActivity(intent);

                s0 = hasilFix;
                s1 = s2 = "";
            } else {
                if (!s0.equals("")) {
                    hasilFix = s0;
                }
            }
        }

        else {
            s1 = btnTxt;
            tvOperasi.setText(s0+s1+s2);
        }
    }
}